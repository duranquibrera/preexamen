/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preexamen;

/**jfutff¿jhjvv
 *
 * @author Javier Duran
 */
public class Pagos {
    
    private int numContrato;
    private String nombreEmp;
    private String domicilio;
    private int tipoCont;
    private int nivelEstu;
    private float pagoDia;
    private int diasTraba;

//Metodos constructores
//Por Omision

public Pagos(){
    this.numContrato = 0;
    this.nombreEmp = "";
    this.domicilio = "";
    this.tipoCont = 0; 
    this.nivelEstu = 0;
    this.pagoDia = 0.0f;
    this.diasTraba = 0;
}

public Pagos(int numContrato, String nombreEmp, String domicilio, int tipoCont, int nivelEstu, float pagoDia, int diasTraba){
    this.numContrato = numContrato;
    this.nombreEmp = nombreEmp;
    this.domicilio = domicilio;
    this.tipoCont = tipoCont; 
    this.nivelEstu = nivelEstu;
    this.pagoDia = pagoDia;
    this.diasTraba = diasTraba;
    }

//Copia

public Pagos(Pagos otro){
    this.numContrato = otro.numContrato;
    this.nombreEmp = otro.nombreEmp;
    this.domicilio = otro.domicilio;
    this.tipoCont = otro.tipoCont; 
    this.nivelEstu = otro.nivelEstu;
    this.pagoDia = otro.pagoDia;
    this.diasTraba = otro.diasTraba;
}

//Metodo get/set

    public int getNumContrato() {
        return numContrato;
    }

    public void setNumContrato(int numContrato) {
        this.numContrato = numContrato;
    }

    public String getNombreEmp() {
        return nombreEmp;
    }

    public void setNombreEmp(String nombreEmp) {
        this.nombreEmp = nombreEmp;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getTipoCont() {
        return tipoCont;
    }

    public void setTipoCont(int tipoCont) {
        this.tipoCont = tipoCont;
    }

    public int getNivelEstu() {
        return nivelEstu;
    }

    public void setNivelEstu(int nivelEstu) {
        this.nivelEstu = nivelEstu;
    }

    public float getPagoDia() {
        return pagoDia;
    }

    public void setPagoDia(float pagoDia) {
        this.pagoDia = pagoDia;
    }

    public int getDiasTraba() {
        return diasTraba;
    }

    public void setDiasTraba(int diasTraba) {
        this.diasTraba = diasTraba;
    }
 
    public float calcularSubtotal (){
        float subto = 0.0f;
        subto = pagoDia * diasTraba;
        switch (nivelEstu){
            case 1: subto = subto * 1.2f ; break;
            case 2: subto = subto * 1.5f ; break;
            case 3: subto = subto * 2 ; break;
        }
        return subto;
    }
    
    public float calcularImpuesto(){
        float impuesto = 0.0f;
        impuesto = calcularSubtotal() * 0.16f;
        return impuesto;
    }
    
    public float calcularTotal(){
        float total = 0.0f;
        total = calcularSubtotal() - calcularImpuesto();
        return total;
    }
}
