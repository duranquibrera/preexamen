/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preexamen;

/**
 *
 * @author Javier Duran
 */
public class PagosMetodos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Pagos pagos = new Pagos();
        
        pagos.setNumContrato(102);
        pagos.setNombreEmp("Jose Lopez");
        pagos.setDomicilio("Av del Sol 1200");
        pagos.setTipoCont(1);
        pagos.setNivelEstu(1);
        pagos.setPagoDia(700.00f);
        pagos.setDiasTraba(15);
        
        // Mostrar información 
        System.out.println("Pago Nominal:");
        System.out.println("Num. Contrato : " + pagos.getNumContrato());
        System.out.println("Nombre Empleado : " + pagos.getNombreEmp());
        System.out.println("Domicilio : " + pagos.getDomicilio());
        System.out.println("Tipo de Contrato : " + pagos.getTipoCont());
        System.out.println("Nivel de Estudios : " + pagos.getNivelEstu());
        System.out.println("Pago Diario Base : " + pagos.getPagoDia());
        System.out.println("Dias Trabajados : " + pagos.getDiasTraba());
        
        System.out.println("Sub Total : " + pagos.calcularSubtotal());
        System.out.println("Impuesto : " + pagos.calcularImpuesto());
        System.out.println("Total Pagar : " + pagos.calcularTotal());
        
        System.out.println("Cotización por Asignacion y Copia:");
        Pagos pag = new Pagos (103,"Maria Acosta","Av del Sol 1200",2,2,700.00f,15);
        
        System.out.println("Num. Contrato : " + pag.getNumContrato());
        System.out.println("Nombre Empleado : " + pag.getNombreEmp());
        System.out.println("Domicilio : " + pag.getDomicilio());
        System.out.println("Tipo de Contrato : " + pag.getTipoCont());
        System.out.println("Nivel de Estudios : " + pag.getNivelEstu());
        System.out.println("Pago Diario Base : " + pag.getPagoDia());
        System.out.println("Dias Trabajados : " + pag.getDiasTraba());
        
        Pagos pag2 = new Pagos (pag);
        
        System.out.println("Sub Total : " + pag2.calcularSubtotal());
        System.out.println("Impuesto : " + pag2.calcularImpuesto());
        System.out.println("Total Pagar : " + pag2.calcularTotal());
    }
    
}
